.. NDPTC API Documentation documentation master file, created by
   sphinx-quickstart on Thu Feb  6 08:38:27 2014.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to NDPTC API Documentation's documentation!
===================================================

Contents:

.. toctree::
   :maxdepth: 2


.. autoclass:: ndptc.deliveries.models.Delivery


Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`

