from django import forms
from ndptc.accounts.models import NDPTCPerson, DMSPerson
from uuid import uuid4
from ndptc.utilities.functions import get_random_word


class NDPTCPeopleForm(forms.ModelForm):
    class Meta:
        exclude = ['is_generated']
        model = NDPTCPerson

    def clean_username(self):
        username = self.cleaned_data['username']
        try:
            NDPTCPerson.objects.get(username=username)
            raise forms.ValidationError('That username already exists')
        except NDPTCPerson.DoesNotExist:
            return username

    def clean_email(self):
        email = self.cleaned_data['email']
        try:
            NDPTCPerson.objects.get(email=email)
            raise forms.ValidationError('That email is already in use')
        except NDPTCPerson.DoesNotExist:
            return email


class DMSPersonForm(forms.ModelForm):
    class Meta:
        fields = ['email', 'first_name', 'last_name', 'middle', 'fema_id', 'title', 'agency', 'address', 'address1',
                  'city', 'state', 'zip', 'country', 'phone', 'government_level', 'discipline', 'citizen']
        model = DMSPerson

        widgets = {
            'email': forms.TextInput(attrs={'class': 'span6'}),
            'last_name': forms.TextInput(attrs={'class': 'span6'}),
            'first_name': forms.TextInput(attrs={'class': 'span6'}),
            'middle': forms.TextInput(attrs={'class': 'span1'}),
            'fema_id': forms.TextInput(attrs={'class': 'span2'}),
            'title': forms.TextInput(attrs={'class': 'span6'}),
            'agency': forms.TextInput(attrs={'class': 'span6'}),
            'government_level': forms.Select(attrs={'class': 'span6'}),
            'discipline': forms.Select(attrs={'class': 'span6'}),
            'phone': forms.TextInput(attrs={'class': 'span3'}),
            'address': forms.TextInput(attrs={'class': 'span6'}),
            'address1': forms.TextInput(attrs={'class': 'span6'}),
            'city': forms.TextInput(attrs={'class': 'span3'}),
            'state': forms.Select(attrs={'class': 'span6'}),
            'zip': forms.TextInput(attrs={'class': 'span3'}),
            'country': forms.Select(attrs={'class': 'span6'}),
        }

    def clean_email(self):
        try:
            DMSPerson.objects.get(email__iexact=self.cleaned_data['email'])
            if self.instance.pk and self.instance.email.lower() != self.cleaned_data['email'].lower():
                raise forms.ValidationError(
                    'The email address is already in use.',
                    code='email_exists',
                )
            return self.cleaned_data['email']
        except DMSPerson.DoesNotExist:
            return self.cleaned_data['email']

    def save(self, commit=True):
        if not self.instance.pk:
            self.instance.username = str(uuid4())
            self.instance.set_unusable_password()
            self.instance.is_active = False
            self.instance.is_generated = True
        return super(DMSPersonForm, self).save(commit)

    def clean_username(self):
        username = self.cleaned_data['username']
        try:
            people = NDPTCPerson.objects.all()
            if self.instance.pk:
                people = people.exclude(pk=self.instance.pk)
            people.get(username=username)
            raise forms.ValidationError('That username already exists')
        except NDPTCPerson.DoesNotExist:
            return username

    def clean_email(self):
        email = self.cleaned_data['email']
        try:
            people = NDPTCPerson.objects.all()
            if self.instance.pk:
                people = people.exclude(pk=self.instance.pk)
            people.get(email=email)
            raise forms.ValidationError('That email is already in use')
        except NDPTCPerson.DoesNotExist:
            return email


class DMSPersonWithUserForm(forms.ModelForm):
    class Meta:
        fields = ['email', 'first_name', 'last_name', 'middle', 'fema_id', 'title', 'agency', 'address', 'address1',
                  'city', 'state', 'zip', 'country', 'phone', 'government_level', 'discipline', 'username', 'citizen']
        model = DMSPerson

        widgets = {
            'username': forms.TextInput(attrs={'class': 'span6'}),
            'email': forms.TextInput(attrs={'class': 'span6'}),
            'last_name': forms.TextInput(attrs={'class': 'span6'}),
            'first_name': forms.TextInput(attrs={'class': 'span6'}),
            'middle': forms.TextInput(attrs={'class': 'span1'}),
            'fema_id': forms.TextInput(attrs={'class': 'span2'}),
            'title': forms.TextInput(attrs={'class': 'span6'}),
            'agency': forms.TextInput(attrs={'class': 'span6'}),
            'government_level': forms.Select(attrs={'class': 'span6'}),
            'discipline': forms.Select(attrs={'class': 'span6'}),
            'phone': forms.TextInput(attrs={'class': 'span3'}),
            'address': forms.TextInput(attrs={'class': 'span6'}),
            'address1': forms.TextInput(attrs={'class': 'span6'}),
            'city': forms.TextInput(attrs={'class': 'span3'}),
            'state': forms.Select(attrs={'class': 'span6'}),
            'zip': forms.TextInput(attrs={'class': 'span3'}),
            'country': forms.Select(attrs={'class': 'span6'}),
        }

    def clean_username(self):
        try:
            DMSPerson.objects.get(username__iexact=self.cleaned_data['username'])
            raise forms.ValidationError(
                'The username already exists.',
                code='user_exists',
            )
        except DMSPerson.DoesNotExist:
            return self.cleaned_data['username']

    def save(self, commit=True):
        if not self.instance.pk:
            self.instance.set_unusable_password()
        return super(DMSPersonWithUserForm, self).save(commit)

    def clean_username(self):
        username = self.cleaned_data['username']
        try:
            NDPTCPerson.objects.get(username=username)
            raise forms.ValidationError('That username already exists')
        except NDPTCPerson.DoesNotExist:
            return username

    def clean_email(self):
        email = self.cleaned_data['email']
        try:
            NDPTCPerson.objects.get(email=email)
            raise forms.ValidationError('That email is already in use')
        except NDPTCPerson.DoesNotExist:
            return email


class UserNameForm(forms.ModelForm):
    class Meta:
        model = DMSPerson
        fields = ('username',)

    def clean_username(self):
        try:
            DMSPerson.objects.get(username__iexact=self.cleaned_data['username'])
            raise forms.ValidationError(
                'The username already exists.',
                code='user_exists',
            )
        except DMSPerson.DoesNotExist:
            return self.cleaned_data['username']

    def save(self, password, commit=True):
        self.instance.is_generated = False
        self.instance.is_active = True
        self.instance.set_password(password)
        return super(UserNameForm, self).save(commit)