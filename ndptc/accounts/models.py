import re
from datetime import timedelta
from django.utils import timezone
from django.core import validators
from django.contrib.auth.models import AbstractBaseUser, PermissionsMixin
from django.core.mail import send_mail
from django.utils.encoding import python_2_unicode_compatible
from django.utils.translation import ugettext_lazy as _
from django.conf import settings
from django.contrib.auth.models import UserManager
from django.contrib.auth.tokens import default_token_generator as token_generator
from django.db import models
from django.template.loader import render_to_string
from django.utils.crypto import random
from django.utils.datetime_safe import datetime
from django.utils.http import int_to_base36
from django.utils.timezone import utc
from hashlib import sha1 as sha_constructor
from ndptc.instructors.models import Instructor
from ndptc.utilities.models import State, Country, GovernmentLevel, Discipline
from six import string_types
import reversion

SHA1_RE = re.compile('^[a-f0-9]{40}$')
ACTIVATED = u"ALREADY_ACTIVATED"


@python_2_unicode_compatible
class NDPTCPerson(AbstractBaseUser, PermissionsMixin):
    """
    """
    username = models.CharField(_('username'), max_length=255, unique=True,
                                help_text=_('Required. 255 characters or fewer. Letters, numbers and '
                                            '@/./+/-/_ characters'),
                                validators=[
                                    validators.RegexValidator(re.compile('^[\w.@+-]+$'), _('Enter a valid username.'),
                                                              'invalid')
                                ])
    first_name = models.CharField(_('first name'), max_length=50)
    last_name = models.CharField(_('last name'), max_length=50)
    email = models.EmailField(_('email address'))#, unique=True)
    is_active = models.BooleanField(_('active'), default=True,
                                    help_text=_('Designates whether this user should be treated as '
                                                'active. Unselect this instead of deleting accounts.'))
    is_uh = models.BooleanField(_('uh user'), default=False,
                                help_text=_('Designates whether this user should be treated as '
                                            'a University of Hawaii user.'))
    is_generated = models.BooleanField(default=False)
    date_joined = models.DateTimeField(_('date joined'), default=datetime.utcnow().replace(tzinfo=utc))

    objects = UserManager()

    USERNAME_FIELD = 'username'
    REQUIRED_FIELDS = ['email']

    class Meta:
        verbose_name = _('user')
        verbose_name_plural = _('users')
        db_table = 'ndptc_person'

    def get_full_name(self):
        """
        Returns the first_name plus the last_name, with a space in between.
        """
        full_name = '{0} {1}'.format(self.first_name, self.last_name)
        return full_name.strip()

    def get_short_name(self):
        """
        Returns the short name for the user.
        """
        return self.first_name

    def get_username(self):
        if self.is_generated:
            return None
        return self.username

    def email_user(self, subject, message, from_email=None):
        """
        Sends an email to this User.
        """
        send_mail(subject, message, from_email, [self.email])

    def __str__(self):  # pragma: no coverage
        return ''#"{0} {1}".format(self.first_name, self.last_name)
reversion.register(NDPTCPerson)


@python_2_unicode_compatible
class DMSPerson(NDPTCPerson):
    #: The persons middle initial. See RES Page 7. Table 4, Row: 4
    middle = models.CharField(max_length=1, null=True, blank=True)
    #: Title within the an organization. See RES Page 7. Table 4, Row: 6
    title = models.CharField(max_length=100, null=True, blank=True)
    #: The person's agency's name. See RES Page 7. Table 4, Row: 5
    agency = models.CharField(max_length=100, null=True, blank=True)
    #: Foreign key to an address. See RES Page 7. Table 4, Row: 7-13.
    address = models.CharField(max_length=100)
    address1 = models.CharField(max_length=100, null=True, blank=True)
    city = models.CharField(max_length=30)
    state = models.ForeignKey(State)
    zip = models.CharField(max_length=25)
    country = models.ForeignKey(Country)
    #: The person's work phone. See RES Page 7. Table 4, Row: 14
    phone = models.CharField(max_length=25)
    #: The students discipline. See RES Page 7. Table 4, Row: 17.
    #: Also see Discipline class in FEMA module.
    government_level = models.ForeignKey(GovernmentLevel)
    #: The person's level in government. See RES Page 7. Table 4, Row: 16.
    #: Also see GovernemntLevel class in FEMA module.
    discipline = models.ForeignKey(Discipline)
    #: An ID that is created from CDP
    fema_id = models.CharField(max_length=10, null=True, blank=True)
    #: Added field used to filter citizens vs. non-citizens.
    citizen = models.BooleanField(default=True)

    objects = UserManager()

    class Meta:
        db_table = 'ndptc_dms_person'
        ordering = ('last_name', 'first_name')

    def get_address(self):
        return '{0} {1}\n{2}, {3}. {4}'.format(self.address, self.address1, self.city, self.state, self.zip)

    def is_instructor(self):
        try:
            Instructor.objects.active().get(person=self)
            return True
        except Instructor.DoesNotExist:
            return False

    def __str__(self):
        return self.get_full_name()
reversion.register(DMSPerson)


# This is the Aku person. It needs to live here so that Moi can create users more easily.
class Person(DMSPerson):
    is_admin = models.BooleanField(default=False, blank=True)
    is_staff = models.BooleanField(default=False)

    class Meta:
        db_table = 'aku_person'

    def get_instructor(self):
        try:
            return Instructor.objects.active().get(person=self.dmsperson)
        except Instructor.DoesNotExist:
            return None

    # TODO: This should really be unit tested.
    def email_help(self):  # pragma: no coverage
        if self.is_active:
            context = {
                'user': self,
                'uid': int_to_base36(self.id),
                'token': token_generator.make_token(self),
            }
            message = render_to_string('emails/password_reset.txt', context)
            return self.email_user('NDPTC Password Reset', message, settings.DEFAULT_FROM_EMAIL)
        else:
            # updated the date_joined field so that activation can happen after multiple days.
            self.date_joined = datetime.utcnow().replace(tzinfo=utc)
            self.save()

            # reconstruct the key.
            salt = sha_constructor(str(random.random()).encode('utf-8')).hexdigest()[:5]
            username = self.username.encode('utf-8')
            activation_key = sha_constructor(salt.encode('utf-8')+username).hexdigest()

            # Send the email
            try:
                profile = RegistrationProfile.objects.get(user=self)
                profile.activation_key = activation_key
            except RegistrationProfile.DoesNotExist:
                profile = RegistrationProfile.objects.create(user=self, activation_key=activation_key)
            profile.save()
            return profile.send_activation_email()
    objects = UserManager()


class RegistrationProfile(models.Model):
    user = models.ForeignKey(Person, unique=True, verbose_name='user')
    activation_key = models.CharField('activation key', max_length=40)

    class Meta:
        db_table = 'aku_registration_profile'

    def __str__(self):  # pragma: no coverage
        return "Registration information for {0}".format(self.user.username)

    def activation_key_expired(self):
        expiration_date = timedelta(days=settings.ACCOUNT_ACTIVATION_DAYS)
        return self.activation_key == ACTIVATED or False
#            (self.user.date_joined + expiration_date > datetime.utcnow().replace(tzinfo=utc))
        # TODO: FIX THIS. I HAVE NO IDEA WHEN IT WENT BAD BUT IT IS FOR SOME REASON. HONESTLY
        # WHO CARES ABOUT THE ACTIVATION DAYS THOUGH.

    # TODO: This should be unit tested
    def send_activation_email(self):  # pragma: no coverage
        context = {
            'activation_key': self.activation_key,
            'expiration_days': settings.ACCOUNT_ACTIVATION_DAYS,
            'user': self.user
        }
        message = render_to_string('emails/activation.txt', context)
        return self.user.email_user('NDPTC Account Activation', message, settings.DEFAULT_FROM_EMAIL)

    @staticmethod
    def activate_user(activation_key):
        if SHA1_RE.search(activation_key):
            try:
                profile = RegistrationProfile.objects.get(activation_key=activation_key)
            except RegistrationProfile.DoesNotExist:
                return False
            if not profile.activation_key_expired():
                user = profile.user
                user.is_active = True
                user.save()
                profile.activation_key = ACTIVATED
                profile.save()
                return user
        return False

    @staticmethod
    def delete_expired_users():
        for profile in RegistrationProfile.objects.all():
            if profile.activation_key_expired():
                user = profile.user
                if not user.is_active:
                    user.delete()
