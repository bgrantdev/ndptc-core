from django.contrib import messages
from django.shortcuts import get_object_or_404
from django.views.generic.base import TemplateView
from ndptc.accounts.forms import DMSPersonForm
from ndptc.accounts.models import DMSPerson


class DMSPeopleView(TemplateView):
    template_name = 'dmspeople.html'

    def get_context_data(self, **kwargs):
        return {
            'form': self.form,
            'person': self.person,
        }

    def dispatch(self, request, pk, *args, **kwargs):
        self.person = get_object_or_404(DMSPerson, pk=pk)
        self.form = DMSPersonForm(self.request.POST or None, instance=self.person)
        return super(DMSPeopleView, self).dispatch(request, *args, **kwargs)

    def post(self, request, *args, **kwargs):
        if self.form.is_valid():
            self.form.save()
            messages.success(request, 'The person has been saved')
        return self.render_to_response(self.get_context_data())
