LEVEL_THREE_CHOICES = (
    (1, 'Very'),
    (2, 'To Some Extent'),
    (3, 'Not'),
)

LEVEL_THREE_QUESTIONS = [
    '1. On a scale of 1-10, how beneficial was the course? 1=no benefit/value and 10=extremely beneficial/valuable',
    '2. What is the number of employees in your organization who could use what is learned in this course?',
    '3. What was the most useful information or skill you took away from the course?',
    '4. Do you feel better prepared after taking this course? Yes/No Please explain',
    '5. Have you shared any information or skills presented in the course with other employees of your organization?',
    '6. Have you used or applied any information or skills presented in the course in day-to-day work tasks, training or in general? Yes or No',
    'a. It was not practical for my situation.',
    'b. There has been no funding/insufficient equipment or resources for additional training.',
    'c. There have been no incidents since the training.',
    'd. Other factors may have prevented you from using the training.',
    '7. Have you or your organization improved or developed any plans or procedures as result of this training?',
    '8. In the area of national domestic preparedness, what type of training is most needed by you or your department?',
    '9. What is your current job duty?',
    '10. Would you be interested in receiving an electronic version of our course catalog?',
    '11. Do you think your feedback is beneficial to our programs?',
    '12. In our continuing effort to improve our programs, we would like to get your immediate supervisor to get his/her feedback on this training. Would you be willing to provide us with your supervisors contact information?',
]