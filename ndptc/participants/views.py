from django.contrib import messages
from django.shortcuts import redirect, get_object_or_404, render_to_response
from django.views.generic.base import View
from .models import WaitList


class AddToDeliveryFromWaitListBaseView(View):
    def dispatch(self, request, pk, *args, **kwargs):
        self.participant = get_object_or_404(WaitList, pk=pk)
        if not self.view_check():
            return redirect(self.fail_url())
        try:
            self.participant.delivery.pop_wait_list(pk)
        except WaitList.DoesNotExist:
            # This really should not happen, if it does it could be a problem # LOGGING
            messages.error(request, 'The participant was not on the wait list')
            return redirect(self.fail_url())
        messages.success(request, 'The participant has been added to the roster')
        return redirect(self.success_url())

    def view_check(self):
        return True

    def fail_url(self):
        return None

    def success_url(self):
        return None


class RemoveFromWaitListBaseView(View):
    def dispatch(self, request, pk, *args, **kwargs):
        self.participant = get_object_or_404(WaitList, pk=pk)
        if not self.view_check():
            return redirect(self.fail_url())
        self.participant.delete()
        messages.success(request, 'The participant has been removed from the wait list')
        return redirect(self.success_url())

    def view_check(self):
        return True

    def fail_url(self):
        return None

    def success_url(self):
        return None
