import pickle
import random
from django.conf import settings
if settings.USE_CERTIFICATES:
    from six import StringIO
    from reportlab.pdfgen import canvas
    from reportlab.lib.pagesizes import letter, landscape
    from reportlab.pdfbase import pdfmetrics
    from reportlab.pdfbase.ttfonts import TTFont
    from django.http import HttpResponse


    def certificates_for_participants(participants):
        pdfmetrics.registerFont(TTFont('TimesNew', '/usr/share/fonts/truetype/msttcorefonts/Times_New_Roman.ttf'))
        pdfmetrics.registerFont(TTFont('TimesNewBold', '/usr/share/fonts/truetype/msttcorefonts/Times_New_Roman_Bold.ttf'))
        pdfmetrics.registerFont(
            TTFont('TimesNewItal', '/usr/share/fonts/truetype/msttcorefonts/Times_New_Roman_Italic.ttf'))

        buffer = StringIO()
        size = landscape(letter)
        c = canvas.Canvas(buffer, pagesize=size)

        if participants.count < 1:
            return None

        course_name = split_string(c, size[0], participants[0].delivery.course.name)
        course_name_shy = split_string(c, size[0] - 60, participants[0].delivery.course.name)

        for participant in participants:
            date = participant.delivery.start.strftime("%B %d, %Y")
            if participant.person.citizen:
                certificate(c, size, '{0} {1}'.format(participant.person.first_name.encode('utf-8'),
                                                      participant.person.last_name.encode('utf-8')), course_name, date)
            else:
                certificate_non_citizen(c, size,
                                        '{0} {1}'.format(participant.person.first_name.encode('utf-8'),
                                                         participant.person.last_name.encode('utf-8')), course_name_shy,
                                        date)
        c.save()
        return buffer


    def certificates_response_for_participants(participants):
        buffer = certificates_for_participants(participants)
        response = HttpResponse(mimetype='application/pdf')
        response['Content-Disposition'] = 'attachment; filename=certificate.pdf'
        buffer.seek(0)
        response.write(buffer.read())
        buffer.close()
        return response


    def certificate(canvas, size, name, course, date_str):
        width, height = size
        half_width = width / 2

        # Images
        canvas.drawImage(settings.DATA_ROOT + 'KarlKimSignature.png', 310, height - 445, 180, 80)
        canvas.drawImage(settings.DATA_ROOT + 'FEMA.png', 25, 20, 150, 131)
        canvas.drawImage(settings.DATA_ROOT + 'ndptc-logo.png', 265, 15, 235, 121)
        canvas.drawImage(settings.DATA_ROOT + 'NDPC_Logo.png', width - 145, 15, 115, 116)

        # THE LINE BELOW
        canvas.setLineWidth(.25)
        canvas.line(275, height - 430, 515, height - 430)

        # NDPTC
        canvas.setFillColorRGB(.14, .23, .58)
        canvas.setFont("TimesNewBold", 36)
        canvas.drawCentredString(half_width, height - 60, "National Disaster Preparedness Training Center")

        # CERT OF COMPLETION
        canvas.setFont("TimesNew", 32)
        canvas.setFillColorRGB(0, 0, 0)
        canvas.drawCentredString(half_width, height - 115, "Certificate of Completion")

        # Awarded To
        canvas.setFont("TimesNewItal", 18)
        canvas.drawCentredString(half_width, height - 150, "presented to")

        # PERSONS NAME
        canvas.setFont("TimesNewBold", 36)
        canvas.drawCentredString(half_width, height - 200, name)

        # Completing
        canvas.setFont("TimesNewItal", 18)
        canvas.drawCentredString(half_width, height - 240, "for successfully completing")

        # THE COURSE, CHECK IF THERE ARE TWO LINES
        canvas.setFont("TimesNew", 36)
        if not course[1]:
            canvas.drawCentredString(half_width, height - 288, course[0])
        else:
            canvas.drawCentredString(half_width, height - 273, course[0])
            canvas.drawCentredString(half_width, height - 308, course[1])

        # ON THIS DAY
        canvas.setFont("TimesNewItal", 18)
        canvas.drawCentredString(half_width, height - 328, "on this day")

        # THE DATE
        canvas.setFont("TimesNewItal", 18)
        canvas.drawCentredString(half_width, height - 350, date_str)

        # Karl Kim
        canvas.setFont("TimesNew", 15)
        canvas.drawCentredString(half_width, height - 445, "Karl Kim, Executive Director")

        # NDPTC AGAIN
        canvas.setFont("TimesNew", 12)
        canvas.drawCentredString(half_width, height - 460, "National Disaster Preparedness Training Center")

        canvas.showPage()


    def certificate_non_citizen(canvas, size, name, course, date_str):
        width, height = size
        half_width = width / 2

        #Images
        canvas.drawImage(settings.DATA_ROOT + 'ndptc-logo.png', 287, height - 150, 220, 116)
        canvas.drawImage(settings.DATA_ROOT + 'ndptc-text.jpg', 178, height - 210, 425, 62)
        canvas.drawImage(settings.DATA_ROOT + 'ndptc-small.jpg', 260, height - 572, 260, 17)
        canvas.drawImage(settings.DATA_ROOT + 'KarlKimSignature.png', 300, height - 542, 180, 80)
        canvas.drawImage(settings.DATA_ROOT + 'left-border.jpg', 35, 31, 19, 550)
        canvas.drawImage(settings.DATA_ROOT + 'right-border.jpg', width - 54, 31, 19, 550)
        canvas.drawImage(settings.DATA_ROOT + 'top-border.jpg', 54, 573, width - 108, 8)
        canvas.drawImage(settings.DATA_ROOT + 'bottom-border.jpg', 54, 31, width - 108, 8)
        canvas.drawImage(settings.DATA_ROOT + 'uh.jpg', 277, height - 233, 235, 22)
        canvas.drawImage(settings.DATA_ROOT + 'certificate_attendance.jpg', 152, height - 268, 477, 26)

        # Awarded To
        canvas.setFont("TimesNewItal", 18)
        canvas.drawCentredString(half_width, height - 285, "presented to")

        # PERSONS NAME
        canvas.setFont("TimesNewBold", 36)
        canvas.drawCentredString(half_width, height - 325, name)

        # Completing
        canvas.setFont("TimesNewItal", 18)
        canvas.drawCentredString(half_width, height - 358, "for successfully completing")

        # THE COURSE, CHECK IF THERE ARE TWO LINES
        canvas.setFont("TimesNew", 36)
        if not course[1]:
            canvas.drawCentredString(half_width, height - 405, course[0])
        else:
            canvas.drawCentredString(half_width, height - 393, course[0])
            canvas.drawCentredString(half_width, height - 422, course[1])

        # ON THIS DAY
        canvas.setFont("TimesNewItal", 18)
        canvas.drawCentredString(half_width, height - 445, "on this day")

        # THE DATE
        canvas.setFont("TimesNewItal", 18)
        canvas.drawCentredString(half_width, height - 470, date_str)

        # Karl Kim
        canvas.setFont("TimesNew", 16)
        canvas.drawCentredString(half_width, height - 545, "Karl Kim, Executive Director")

        # THE LINE BELOW
        canvas.setLineWidth(.25)
        canvas.line(275, height - 525, 500, height - 525)

        canvas.showPage()


    def split_string(canvas, width, course_name):
        """
        "" This function will split the course name into two parts if needed
        "" If the font or font size changes above, the value must be changed here as well.
        """
        canvas.setFont("TimesNew", 36)
        if canvas.stringWidth(course_name) < width:
            return (course_name, None)
        length = len(course_name)
        middle = length / 2
        for i in range(middle, length):
            if course_name[i] == ' ':
                first = course_name[0:i]
                second = course_name[i:length]
                if canvas.stringWidth(first) < width and canvas.stringWidth(second) < width:
                    return (first, second)
                else:
                    break


def get_random_word():
    """
    This function returns a random word from the ndptc word bank.
    """

    data_root = settings.DATA_ROOT
    words = open(data_root + 'word_list', 'r')
    words = pickle.load(words)
    word = random.choice(words)
    return word

def grouped(l, n):
    for i in xrange(0, len(l), n):
        yield l[i:i+n]
