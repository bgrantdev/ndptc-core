from django.contrib.contenttypes.models import ContentType
from django.core.urlresolvers import reverse
from django.shortcuts import redirect
from django.views.generic import TemplateView
from ndptc.accounts.mixins import LoginRequiredMixin

import reversion


class AuditModelView(LoginRequiredMixin, TemplateView):
    app_label = None
    model = None
    template_name = None
    redirect_url_name = 'home'

    def get_context_data(self, **kwargs):
        return {
            'model': self.model,
            'versions': self.versions
        }

    def dispatch(self, request, pk, *args, **kwargs):
        try:
            content_type = ContentType.objects.get(app_label=self.app_label, model=self.model)
        except ContentType.DoesNotExist:
            return redirect(reverse(self.redirect_url_name))
        self.model = content_type.get_object_for_this_type(pk=pk)
        self.versions = reversion.get_unique_for_object(self.model)
        return super(AuditModelView, self).dispatch(request, *args, **kwargs)