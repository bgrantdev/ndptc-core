from django import forms
from django.conf import settings
from django.forms.widgets import RadioFieldRenderer, CheckboxInput
from django.forms.widgets import RadioSelect
from django.forms.widgets import Select
from django.forms.util import flatatt
from django.utils.html import escape, conditional_escape
from django.utils.dates import MONTHS
from django.utils.encoding import StrAndUnicode, force_unicode
from django.utils.safestring import mark_safe
from django.forms.widgets import ClearableFileInput, Widget, Select
from django.core.urlresolvers import reverse
from calendar import HTMLCalendar
from datetime import date
from ndptc.courses.models import Delivery
import re


RE_DATE = re.compile(r'(\d{4})-(\d\d?)-(\d\d?)$')

UPLOAD_IMG_ID = "new-img-file"

class JcropWidget(forms.Widget):
    class Media:
      # form media, i.e. CSS and JavaScript needed for Jcrop.
      # You'll have to adopt these to your project's paths.
        css = {
               'all': (settings.MEDIA_URL + "css/jquery.Jcrop.css",)
               }
        js = (
              settings.MEDIA_URL + "js/lib/jquery.Jcrop.min.js",
              )
    
    # fixed Jcrop options; to pass options to Jcrop, use the jcrop_options
    # argument passed to the JcropForm constructor. See example above.
    jcrop_options = {
                      "onSelect": "storeCoords",
                      "onChange": "storeCoords",
                    }
  
    # HTML template for the widget. 
    #
    # The widget is constructed from the following parts:
    #
    #  * HTML <img> - the actual image used for displaying and cropping
    #  * HTML <label> and <input type="file> - used for uploading a new
    #                                          image
    #  * HTML <input type="hidden"> - to remember image path and filename
    #  * JS code - The JS code makes the image a Jcrop widget and 
    #              registers an event handler for the <input type="file"> 
    #              widget. The event handler submits the form so the new
    #              image is sent to the server without the user having
    #              to press the submit button.
    # 
    markup = """
      <!--
      <label for="new-img-file">Choose File:</label>
      <input type="file" name="%(UPLOAD_IMG_ID)s" id="%(UPLOAD_IMG_ID)s"/><br/><br/>
      -->
      <img id="jcrop-img" src="%(MEDIA_URL)s%(img_fn)s"/><br/>
      <input type="hidden" name="imagefile" id="imagefile" value="%(imagefile)s"/>
      <script type="text/javascript">
      function storeCoords(c)
      {
        jQuery('#id_x1').val(c.x);
        jQuery('#id_x2').val(c.x2);
        jQuery('#id_y1').val(c.y);
        jQuery('#id_y2').val(c.y2);
      }
      jQuery(function() {
          jQuery('#jcrop-img').Jcrop(%(jcrop_options)s);
          jQuery('#%(UPLOAD_IMG_ID)s').change(function(e){
            var form = jQuery('#%(UPLOAD_IMG_ID)s').parents('form:first');
            form.submit();
          });
      });</script>
    """

    def __init__(self, attrs=None):
        """
        __init__ does nothing special for now
        """
        super(JcropWidget, self).__init__(attrs)
    
    def add_jcrop_options(self, options):
        """
        add jcrop options; options is expected to be a dictionary of name/value
        pairs that Jcrop understands; 
        see http://deepliquid.com/content/Jcrop_Manual.html#Setting_Options
        """
        for k, v in options.items():
            self.jcrop_options[k] = v
    
    def render(self, name, value, attrs=None):
        """
        render the Jcrop widget in HTML
        """
        # translate jcrop_options dictionary to JavaScipt
        jcrop_options = "{";
        for k, v in self.jcrop_options.items():
            jcrop_options = jcrop_options + "%s: %s," % (k, v)
        jcrop_options = jcrop_options + "}"
        
        # fill in HTML markup string with actual data
        output = self.markup % {
                                 "MEDIA_URL": settings.MEDIA_URL,
                                 "img_fn": str(value),
                                 "UPLOAD_IMG_ID": UPLOAD_IMG_ID,
                                 "jcrop_options": jcrop_options,
                                 "imagefile": value,
                               }
        return mark_safe(output)

class RadioInputLabelNoTable(StrAndUnicode):
    """
    An object used by RadioFieldRenderer that represents a single
    <input type='radio'>.
    """

    def __init__(self, name, value, attrs, choice, index):
        self.name, self.value = name, value
        self.attrs = attrs
        self.choice_value = force_unicode(choice[0])
        self.choice_label = force_unicode(choice[1])
        self.index = index

    def __unicode__(self):
        if 'id' in self.attrs:
            label_for = ' for="%s_%s"' % (self.attrs['id'], self.index)
        else:
            label_for = ''
        choice_label = conditional_escape(force_unicode(self.choice_label))
        return mark_safe(u'<td><label>%s %s</label></td>' % (self.tag(), choice_label))

    def is_checked(self):
        return self.value == self.choice_value

    def tag(self):
        if 'id' in self.attrs:
            self.attrs['id'] = '%s_%s' % (self.attrs['id'], self.index)
        final_attrs = dict(self.attrs, type='radio', name=self.name, value=self.choice_value)
        if self.is_checked():
            final_attrs['checked'] = 'checked'
        return mark_safe(u'<input%s />' % flatatt(final_attrs))

class NoListRadioRenderer(RadioFieldRenderer):
    """
    An object used by RadioSelect to enable customization of radio widgets.
    """
    def __init__(self, name, value, attrs, choices):
        self.name, self.value, self.attrs = name, value, attrs
        self.choices = choices

    def __iter__(self):
        for i, choice in enumerate(self.choices):
            yield RadioInputLabelNoTable(self.name, self.value, self.attrs.copy(), choice, i)

    def __getitem__(self, idx):
        choice = self.choices[idx] # Let the IndexError propogate
        return RadioInputLabelNoTable(self.name, self.value, self.attrs.copy(), choice, idx)

    def __unicode__(self):
        return self.render()
    def render(self):
        return mark_safe(u''.join([u'%s' % force_unicode(w) for w in self]))
    
class RadioSelectNoList(RadioSelect):
    renderer = NoListRadioRenderer

class RadioInputNoLabel(StrAndUnicode):
    """
    An object used by RadioFieldRenderer that represents a single
    <input type='radio'>.
    """

    def __init__(self, name, value, attrs, choice, index):
        self.name, self.value = name, value
        self.attrs = attrs
        self.choice_value = force_unicode(choice[0])
        self.choice_label = force_unicode(choice[1])
        self.index = index

    def __unicode__(self):
        if 'id' in self.attrs:
            label_for = ' for="%s_%s"' % (self.attrs['id'], self.index)
        else:
            label_for = ''
        return mark_safe(u'<td style="text-align: center;">%s</td>' % (self.tag()))

    def is_checked(self):
        return self.value == self.choice_value

    def tag(self):
        if 'id' in self.attrs:
            self.attrs['id'] = '%s_%s' % (self.attrs['id'], self.index)
        final_attrs = dict(self.attrs, type='radio', name=self.name, value=self.choice_value)
        if self.is_checked():
            final_attrs['checked'] = 'checked'
        return mark_safe(u'<input%s />' % flatatt(final_attrs))

class NoLabelRadioRenderer(RadioFieldRenderer):
    """
    An object used by RadioSelect to enable customization of radio widgets.
    """
    def __init__(self, name, value, attrs, choices):
        self.name, self.value, self.attrs = name, value, attrs
        self.choices = choices

    def __iter__(self):
        for i, choice in enumerate(self.choices):
            yield RadioInputNoLabel(self.name, self.value, self.attrs.copy(), choice, i)

    def __getitem__(self, idx):
        choice = self.choices[idx] # Let the IndexError propogate
        return RadioInputNoLabel(self.name, self.value, self.attrs.copy(), choice, idx)

    def __unicode__(self):
        return self.render()
    def render(self):
        return mark_safe(u''.join([u'%s' % force_unicode(w) for w in self]))

class RadioSelectNoLabel(RadioSelect):
    renderer = NoLabelRadioRenderer
  
class RadioSelectNoList(RadioSelect):
    renderer = NoListRadioRenderer

class RadioInputTable(StrAndUnicode):
    """
    An object used by RadioFieldRenderer that represents a single
    <input type='radio'>.
    """

    def __init__(self, name, value, attrs, choice, index):
        self.name, self.value = name, value
        self.attrs = attrs
        self.choice_value = force_unicode(choice[0])
        self.choice_label = force_unicode(choice[1])
        self.index = index

    def __unicode__(self):
        if 'id' in self.attrs:
            label_for = ' for="%s_%s"' % (self.attrs['id'], self.index)
        else:
            label_for = ''
        choice_label = conditional_escape(force_unicode(self.choice_label))
        return mark_safe(u'<td>%s</td>\n <td align="left" valign="middle">%s</td>\n' % (self.tag(), choice_label))

    def is_checked(self):
        return self.value == self.choice_value

    def tag(self):
        if 'id' in self.attrs:
            self.attrs['id'] = '%s_%s' % (self.attrs['id'], self.index)
        final_attrs = dict(self.attrs, type='radio', name=self.name, value=self.choice_value)
        if self.is_checked():
            final_attrs['checked'] = 'checked'
        return mark_safe(u'<input%s />' % flatatt(final_attrs))

class RadioFieldRendererTable(StrAndUnicode):
    """
    An object used by RadioSelect to enable customization of radio widgets.
    """

    def __init__(self, name, value, attrs, choices):
        self.name, self.value, self.attrs = name, value, attrs
        self.choices = choices

    def __iter__(self):
        for i, choice in enumerate(self.choices):
            yield RadioInputTable(self.name, self.value, self.attrs.copy(), choice, i)

    def __getitem__(self, idx):
        choice = self.choices[idx] # Let the IndexError propogate
        return RadioInputTable(self.name, self.value, self.attrs.copy(), choice, idx)

    def __unicode__(self):
        return self.render()

    def render(self):
        str = '<tr>\n'
        i = 0
        for i, choice in enumerate(self):
            if i % 3 == 0 and i != 0:
                str += '</tr>\n<tr>\n'
            str += force_unicode(choice)
        if not i % 3 == 0:
            str += '</tr>\n'
        return mark_safe(force_unicode(str))
    

class RadioSelectTable(RadioSelect):
    renderer = RadioFieldRendererTable

class AvailabilityCalendar(HTMLCalendar):
    def __init__(self, dates_available):
        super(AvailabilityCalendar, self).__init__()
        self.dates_available = dates_available

    def formatday(self, day, weekday):
        if day != 0:
            cssclass = self.cssclasses[weekday]
            if date.today() == date(self.year, self.month, day):
                cssclass += ' today'
            if date(self.year, self.month, day) in self.dates_available:
                cssclass += ' on'
            cssclass += ' day'
            return self.day_cell(cssclass, day)
        return self.day_cell('noday', '&nbsp;')

    def formatmonth(self, year, month):
        self.year, self.month = year, month
        return super(AvailabilityCalendar, self).formatmonth(year, month)


    def day_cell(self, cssclass, body):
        return '<td class="%s">%s</td>' % (cssclass, body)

class DocumentClearableFileInput(ClearableFileInput):
    def render(self, name, value, attrs=None):
        substitutions = {
            'initial_text': self.initial_text,
            'input_text': self.input_text,
            'clear_template': '',
            'clear_checkbox_label': self.clear_checkbox_label,
        }
        template = u'%(input)s'
        substitutions['input'] = super(ClearableFileInput, self).render(name, value, attrs)

        if value and hasattr(value, "url"):
            template = self.template_with_initial
            substitutions['initial'] = (u'<a href="%s">%s</a>'
                                        % (reverse('courses_show_document', args=[value.instance.id]),
                                        escape(value.url)))
            if not self.is_required:
                checkbox_name = self.clear_checkbox_name(name)
                checkbox_id = self.clear_checkbox_id(checkbox_name)
                substitutions['clear_checkbox_name'] = conditional_escape(checkbox_name)
                substitutions['clear_checkbox_id'] = conditional_escape(checkbox_id)
                substitutions['clear'] = CheckboxInput().render(checkbox_name, False, attrs={'id': checkbox_id})
                substitutions['clear_template'] = self.template_with_clear % substitutions

        return mark_safe(template % substitutions)
    
class ResumeClearableFileInput(ClearableFileInput):
    def render(self, name, value, attrs=None):
        substitutions = {
            'initial_text': self.initial_text,
            'input_text': self.input_text,
            'clear_template': '',
            'clear_checkbox_label': self.clear_checkbox_label,
        }
        template = u'%(input)s'
        substitutions['input'] = super(ClearableFileInput, self).render(name, value, attrs)

        if value and hasattr(value, "url"):
            template = self.template_with_initial
            substitutions['initial'] = (u'<a href="%s">%s</a>'
                                        % (reverse('Instructor.views.InstructorResume', args=[]),
                                        'View resume.'))
            if not self.is_required:
                checkbox_name = self.clear_checkbox_name(name)
                checkbox_id = self.clear_checkbox_id(checkbox_name)
                substitutions['clear_checkbox_name'] = conditional_escape(checkbox_name)
                substitutions['clear_checkbox_id'] = conditional_escape(checkbox_id)
                substitutions['clear'] = CheckboxInput().render(checkbox_name, False, attrs={'id': checkbox_id})
                substitutions['clear_template'] = self.template_with_clear % substitutions

        return mark_safe(template % substitutions)

class MonthYearWidget(Widget):
    """
    A Widget that splits date input into two <select> boxes for month and year,
    with 'day' defaulting to the first of the month.
    
    Based on SelectDateWidget, in 
    
    django/trunk/django/forms/extras/widgets.py
    
    
    """
    none_value = (0, '---')
    month_field = '%s_month'
    year_field = '%s_year'

    def __init__(self, attrs=None, years=None, required=True):
        # years is an optional list/tuple of years to use in the "year" select box.
        self.attrs = attrs or {}
        self.required = required
        self.years = []
        for year in Delivery.objects.dates('start', 'year'):
            self.years.append(year.year)

    def render(self, name, value, attrs=None):
        try:
            year_val, month_val = value.year, value.month
        except AttributeError:
            year_val = month_val = None
            if isinstance(value, basestring):
                match = RE_DATE.match(value)
                if match:
                    year_val, month_val, day_val = [int(v) for v in match.groups()]

        output = []

        if 'id' in self.attrs:
            id_ = self.attrs['id']
        else:
            id_ = 'id_%s' % name

        month_choices = MONTHS.items()
        if not (self.required and value):
            month_choices.append(self.none_value)
        month_choices.sort()
        local_attrs = self.build_attrs(id=self.month_field % id_)
        s = Select(choices=month_choices)
        select_html = s.render(self.month_field % name, month_val, local_attrs)
        output.append(select_html)

        year_choices = [(i, i) for i in self.years]
        if not (self.required and value):
            year_choices.insert(0, self.none_value)
        local_attrs['id'] = self.year_field % id_
        s = Select(choices=year_choices)
        select_html = s.render(self.year_field % name, year_val, local_attrs)
        output.append(select_html)

        return mark_safe(u'\n'.join(output))

    def id_for_label(self, id_):
        return '%s_month' % id_
    id_for_label = classmethod(id_for_label)

    def value_from_datadict(self, data, files, name):
        y = data.get(self.year_field % name)
        m = data.get(self.month_field % name)
        if y == m == "0":
            return None
        if y and m:
            return '%s-%s-%s' % (y, m, 1)
        return data.get(name, None)
