#!/usr/bin/env python
# -*- coding: utf-8 -*-
import os
import shutil
import sys
import tempfile
import six
import ndptc

urlpatterns = []

TEMPLATE_DEBUG = True
ROOT_DIR = os.path.dirname(os.path.abspath(__file__))
TEMP_DIR = tempfile.mkdtemp(prefix='django_')

DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.sqlite3',
        'NAME': ':memory:'
    },
}


INSTALLED_APPS = [
        'django.contrib.auth',
        'django.contrib.contenttypes',
        'django.contrib.sessions',
        'django.contrib.messages',
        'django.contrib.staticfiles',
        'reversion',

        'ndptc.accounts',
        'ndptc.courses',
        'ndptc.deliveries',
        'ndptc.instructors',
        'ndptc.participants',
        'ndptc.utilities',

        'tests.accounts',
        'tests.courses',
        'tests.deliveries',
        'tests.instructors',
        'tests.participants',

        'django_nose',
]

TEMPLATE_DIRS = [
    os.path.join(os.path.dirname(__file__), 'ndptc', 'tests'),
]


def runtests(verbosity, interactive, failfast, test_labels):
    from django.conf import settings
    settings.configure(
        INSTALLED_APPS=INSTALLED_APPS,
        DATABASES=DATABASES,
        AUTH_USER_MODEL='accounts.NDPTCPerson',
        USE_TZ=True,
        TEST_RUNNER='django_nose.NoseTestSuiteRunner',
        TEMPLATE_DIRS=TEMPLATE_DIRS,
        TEMPLATE_DEBUG=TEMPLATE_DEBUG,
        ACCOUNT_ACTIVATION_DAYS=3,
        USE_CERTIFICATES=False,
        STATIC_ROOT=os.path.join(TEMP_DIR, 'static'),
        DOCUMENT_ROOT=os.path.join(TEMP_DIR, 'static'),
        PASSWORD_HASHERS=(
            'django.contrib.auth.hashers.MD5PasswordHasher',
        ),
        FIXTURE_DIRS = (
            ROOT_DIR + '/ndptc/courses/fixtures/',
            ROOT_DIR + '/ndptc/utilities/fixtures/',
            ROOT_DIR + '/ndptc/accounts/fixtures/',
            ROOT_DIR + '/ndptc/instructors/fixtures/',
        ),
        SECRET_KEY="ndptc_tests_secret_key",
        NOSE_ARGS=[
            '--with-coverage',
            '--cover-package=ndptc.accounts,ndptc.courses,ndptc.deliveries,ndptc.participants,ndptc.instructors,'
            'ndptc.utilities,ndptc.managers'
        ],
        NOSE_TESTS_EXCLUDE = [
            'ndptc.courses.defs',
        ],
    )

    # Run the test suite, including the extra validation tests.
    from django.test.utils import get_runner
    TestRunner = get_runner(settings)
    print("Testing against ndptc installed in '%s'" % os.path.dirname(ndptc.__file__))

    test_runner = TestRunner(verbosity=verbosity, interactive=interactive, failfast=failfast)
    failures = test_runner.run_tests(test_labels)
    return failures


def teardown():
    try:
        shutil.rmtree(six.text_type(TEMP_DIR))
    except OSError:
        print('Failed to remove temp directory: %s' % TEMP_DIR)


if __name__ == "__main__":
    from optparse import OptionParser
    usage = "%prog [options] [module module module ...]"
    parser = OptionParser(usage=usage)
    parser.add_option(
        '-v', '--verbosity', action='store', dest='verbosity', default='1',
        type='choice', choices=['0', '1', '2', '3'],
        help='Verbosity level; 0=minimal output, 1=normal output, 2=all '
             'output')
    parser.add_option(
        '--noinput', action='store_false', dest='interactive', default=True,
        help='Tells Django to NOT prompt the user for input of any kind.')
    parser.add_option(
        '--failfast', action='store_true', dest='failfast', default=False,
        help='Tells Django to stop running the test suite after first failed '
             'test.')
    options, args = parser.parse_args()

    os.environ['DJANGO_SETTINGS_MODULE'] = 'runtests'
    options.settings = os.environ['DJANGO_SETTINGS_MODULE']

    runtests(int(options.verbosity), options.interactive, options.failfast, args)
