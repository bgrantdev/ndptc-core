from ndptc.accounts.models import NDPTCPerson, DMSPerson, Person, RegistrationProfile
from ndptc.utilities.models import State, Country, GovernmentLevel, Discipline
import factory


class NDPTCPersonFactory(factory.django.DjangoModelFactory):
    FACTORY_FOR = NDPTCPerson
    username = factory.Sequence(lambda n: 'user{0}'.format(n))
    email = factory.LazyAttribute(lambda a: 'user.{0}@example.com'.format(a.username).lower())
    first_name = 'test'
    last_name = 'user'


class DMSPersonFactory(factory.django.DjangoModelFactory):
    FACTORY_FOR = DMSPerson
    username = factory.Sequence(lambda n: 'user{0}'.format(n))
    email = factory.LazyAttribute(lambda a: 'user.{0}@example.com'.format(a.username).lower())
    first_name = 'test'
    last_name = 'user'
    middle = 'p'
    title = 'bar'
    agency = 'FooBar'
    address = '123 St'
    address1 = 'Apt 1'
    city = 'Nowhere'
    state = State.objects.get(fema_code='HI')
    zip = '12345'
    country = Country.objects.get(fema_code='US')
    phone = '5555555555'
    government_level = GovernmentLevel.objects.all()[0]
    discipline = Discipline.objects.all()[0]
    fema_id = '0000000000'
    citizen = True
    is_active = False


class PersonFactory(factory.django.DjangoModelFactory):
    FACTORY_FOR = Person
    username = factory.Sequence(lambda n: 'user{0}'.format(n))
    email = factory.LazyAttribute(lambda a: 'user.{0}@example.com'.format(a.username).lower())
    first_name = 'test'
    last_name = 'user'
    middle = 'p'
    title = 'bar'
    agency = 'FooBar'
    address = '123 St'
    address1 = 'Apt 1'
    city = 'Nowhere'
    state = State.objects.get(fema_code='HI')
    zip = '12345'
    country = Country.objects.get(fema_code='US')
    phone = '5555555555'
    government_level = GovernmentLevel.objects.all()[0]
    discipline = Discipline.objects.all()[0]
    fema_id = '0000000000'
    citizen = True
    is_active = False


class RegistrationProfileFactory(factory.django.DjangoModelFactory):
    FACTORY_FOR = RegistrationProfile
    user = factory.SubFactory(PersonFactory)

