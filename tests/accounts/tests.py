from datetime import timedelta
from django.conf import settings
from django_nose import FastFixtureTestCase
from ndptc.accounts.models import RegistrationProfile
from .factories import NDPTCPersonFactory, DMSPersonFactory, PersonFactory, RegistrationProfileFactory
from ..instructors.factories import InstructorFactory


class NDPTCPersonTests(FastFixtureTestCase):
    def setUp(self):
        self.ndptc_user = NDPTCPersonFactory(username='test_user')

    def full_name_test(self):
        self.assertEqual('test user', self.ndptc_user.get_full_name())

    def short_name_test(self):
        self.assertEqual('test', self.ndptc_user.get_short_name())

    def username_test(self):
        self.assertEqual('test_user', self.ndptc_user.get_username())
        self.ndptc_user.is_generated = True
        self.assertEqual(None, self.ndptc_user.get_username())


class DMSPersonTests(FastFixtureTestCase):
    def setUp(self):
        self.user = DMSPersonFactory()

    def full_name_test(self):
        self.assertEqual('123 St Apt 1\nNowhere, Hawaii. 12345', self.user.get_address())

    def is_instructor_test(self):
        self.assertFalse(self.user.is_instructor())
        InstructorFactory(person=self.user)
        self.assertTrue(self.user.is_instructor())

    def str_test(self):
        self.assertEqual('test user', str(self.user))


class PersonTests(FastFixtureTestCase):
    def setUp(self):
        self.user = PersonFactory()

    def get_instructor_test(self):
        self.assertEqual(None, self.user.get_instructor())

    def str_test(self):
        self.assertEqual('test user', str(self.user))


class RegistrationProfileTests(FastFixtureTestCase):
    def setUp(self):
        self.user1 = PersonFactory()
        self.user2 = PersonFactory()
        self.profile1 = RegistrationProfileFactory(user=self.user1,
                                                   activation_key='59b4c6dbd83e2e3eeb99661e5e129f67fb2792ae')
        self.profile2 = RegistrationProfileFactory(user=self.user2,
                                                   activation_key='59b4c6dbd83e2e3eeb99661e5e129f67fb2792aa')

    def delete_expired_users_test(self):
        self.user1.date_joined = self.user1.date_joined - timedelta(days=settings.ACCOUNT_ACTIVATION_DAYS + 1)
        self.user1.save()
        self.assertEqual(2, RegistrationProfile.objects.all().count())
        RegistrationProfile.delete_expired_users()
        self.assertEqual(1, RegistrationProfile.objects.all().count())

    def activate_user_test(self):
        self.assertEqual(False, self.user2.is_active)
        self.user2 = RegistrationProfile.activate_user(self.profile2.activation_key)
        self.assertEqual(True, self.user2.is_active)
        self.assertEqual(False, RegistrationProfile.activate_user('1234'))
        self.assertEqual(False, RegistrationProfile.activate_user('59b4c6dbd83e2e3eeb99661e5e129f67fb2792a1'))

    def email_help_test(self):
        pass
        #raise Exception(str(self.user1.email_help()))
