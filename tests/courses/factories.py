from ndptc.courses.defs import CourseStatus
from ndptc.courses.models import CourseType, TrainingProvider, TrainingType, TestType
import factory


class CourseFactory(factory.django.DjangoModelFactory):
    FACTORY_FOR = 'courses.Course'
    course_type = CourseType.objects.get(code='I')
    status = CourseStatus.NONE
    training_provider = TrainingProvider.objects.get(provider='UH-NDPTC')
    training_type = TrainingType.objects.get(code='AW')
    name = factory.Sequence(lambda c: 'Course {0}'.format(c))
    number = factory.Sequence(lambda n: 'AWR-{0}'.format(n))
    short_name = 'course'


class TestFactory(factory.django.DjangoModelFactory):
    FACTORY_FOR = 'courses.Test'
    course = factory.SubFactory(CourseFactory)
    type = TestType.objects.get(pk='PRE')
    label = factory.Sequence(lambda n: 'Test {0}'.format(n))


class QuestionFactory(factory.django.DjangoModelFactory):
    FACTORY_FOR = 'courses.Question'
    question = factory.Sequence(lambda n: 'Test Question {0}'.format(n))
    test = factory.SubFactory(TestFactory)
    sort_order = factory.Sequence(lambda n: n)


class AnswerFactory(factory.django.DjangoModelFactory):
    FACTORY_FOR = 'courses.Answer'
    answer = factory.Sequence(lambda n: 'Answer {0}'.format(n))
    question = factory.SubFactory(QuestionFactory)
    sort_order = factory.Sequence(lambda n: n)
    correct = False

