from django_nose import FastFixtureTestCase
from ndptc.courses.defs import CourseStatus
from ndptc.courses.models import DocumentType, TrainingType
from ndptc.utilities.models import State
from .factories import CourseFactory, TestFactory, QuestionFactory, AnswerFactory
from ..deliveries.factories import DeliveryFactory, EvaluationFactory
from ..participants.factories import ParticipantFactory, TestResultFactory
from ..instructors.factories import CertificationFactory


class CourseTests(FastFixtureTestCase):

    def test_defaults(self):
        course = CourseFactory(icon2d=None, icon3d=None)
        self.assertEqual(course.is_active, True)
        self.assertEqual(course.certified, False)
        self.assertEqual(course.show_in_catalog(), False)
        self.assertEqual(course.get_number(), '----')
        self.assertEqual(course.get_icon2d(), '/media/icons/icon_default.png')
        self.assertEqual(course.get_icon3d(), '/media/icons/icon_default.png')

    def test_show_in_catalog(self):
        course = CourseFactory(status=CourseStatus.SHOW_IN_CATALOG)
        self.assertEqual(course.show_in_catalog(), True)

    def test_set_active(self):
        course = CourseFactory()
        course.set_active(False)
        self.assertEqual(course.is_active, False)
        for delivery in course.delivery_set.all():
            self.assertEqual(delivery.is_active, False)
        for document in course.document_set.all():
            self.assertEqual(document.is_active, False)
        for test in course.test_set.all():
            self.assertEqual(test.is_active, False)

    def test_get_number(self):
        course = CourseFactory(number='AWR-123')
        self.assertEqual(course.get_number(), '----')
        course.certified = True
        self.assertEqual(course.get_number(), 'AWR-123')

    def test_str(self):
        course = CourseFactory()
        self.assertEqual(str(course), course.name)
        course.certified = True
        self.assertEqual(str(course), ''.join(course.name + ' (' + course.number + ')'))

    def test_yearly_report(self):
        course = CourseFactory()
        delivery = DeliveryFactory(course=course)
        ParticipantFactory(delivery=delivery)
        ParticipantFactory(delivery=delivery, attended=True, passed=False)
        ParticipantFactory(delivery=delivery, attended=True, passed=True)
        report = course.yearly_report()[0]
        self.assertEqual(report['year'], delivery.start.year)
        self.assertEqual(3, report['registered'])
        self.assertEqual(2, report['attended'])
        self.assertEqual(1, report['passed'])
        self.assertEqual(1, report['failed'])

    def test_participants_by_state_report(self):
        course = CourseFactory()
        delivery = DeliveryFactory(course=course)
        participant1 = ParticipantFactory(delivery=delivery)
        ParticipantFactory(delivery=delivery, attended=True, passed=False)
        ParticipantFactory(delivery=delivery, attended=True, passed=True)
        participant2 = ParticipantFactory(delivery=delivery, person__state=State.objects.get(fema_code='WA'))
        report = course.participants_by_state_report()[0]
        self.assertEqual(report['state'], str(participant1.person.state))
        self.assertEqual(3, report['registered'])
        self.assertEqual(2, report['attended'])
        self.assertEqual(1, report['passed'])
        self.assertEqual(1, report['failed'])
        report = course.participants_by_state_report()[1]
        self.assertEqual(report['state'], str(participant2.person.state))

    # TODO: This test does not quite test this report correctly.
    def instructors_by_state_report_test(self):
        course = CourseFactory()
        [CertificationFactory(instructor__person__state=state, course=course) for state in State.objects.all()]
        report = course.instructors_by_state_report()
        [self.assertEqual(1, report[i]['count']) for i in range(0, State.objects.all().count())]

    def evaluation_report_test(self):
        course = CourseFactory()
        EvaluationFactory(delivery__course=course)
        report = course.evaluation_report()
        for i, section in enumerate(report):
            if i == 0:
                self.assertEqual(1, section['results'][0][1])
                self.assertEqual(1, section['results'][1][2])
            if i == 1:
                self.assertEqual(1, section['results'][0][3])
                self.assertEqual(1, section['results'][1][4])
                self.assertEqual(1, section['results'][2][5])
                self.assertEqual(1, section['results'][3][0])
            if i == 2:
                self.assertEqual(1, section['results'][0][1])
                self.assertEqual(1, section['results'][1][2])
                self.assertEqual(1, section['results'][2][3])
                self.assertEqual(1, section['results'][3][4])
                self.assertEqual(1, section['results'][4][5])
                self.assertEqual(1, section['results'][5][0])
                self.assertEqual(1, section['results'][6][1])
                self.assertEqual(1, section['results'][7][2])
            if i == 3:
                self.assertEqual(1, section['results'][0][3])
                self.assertEqual(1, section['results'][1][4])
                self.assertEqual(1, section['results'][2][5])
                self.assertEqual(1, section['results'][3][0])
                self.assertEqual(1, section['results'][4][1])
            if i == 4:
                self.assertEqual(1, section['results'][0][2])
                self.assertEqual(1, section['results'][1][3])
                self.assertEqual(1, section['results'][2][4])
                self.assertEqual(1, section['results'][3][5])
            if i == 5:
                for i, comment in enumerate(section['results']):
                    self.assertEqual('Question {0}'.format(i+24), comment[0][0])


class TestTests(FastFixtureTestCase):
    def setUp(self):
        self.course = CourseFactory()
        self.test = TestFactory(course=self.course, label='Test')

    def str_test(self):
        question = QuestionFactory(question='Test Question')
        answer = AnswerFactory(question=question, answer='Test Answer')
        self.assertEqual('Test Question', str(question))
        self.assertEqual('Test Answer', str(answer))
        self.assertEqual('Test', str(self.test))

    def is_active_test(self):
        self.test.set_active(False)
        self.assertEqual(False, self.test.is_active)
        self.test.set_active(True)
        self.assertEqual(True, self.test.is_active)

    def has_data_test(self):
        question = QuestionFactory(test=self.test)
        answer = AnswerFactory(question=question)
        TestResultFactory(question=question, answer=answer)
        self.assertTrue(self.test.has_data())
        test = TestFactory()
        self.assertFalse(test.has_data())


class LookUpTableTests(FastFixtureTestCase):

    def document_type_str_test(self):
        document_type = DocumentType.objects.get(code='PG')
        self.assertEqual('Participant Guide', str(document_type))

    def training_type_str_test(self):
        training_type = TrainingType.objects.get(code='AW')
        self.assertEqual('Awareness', str(training_type))
