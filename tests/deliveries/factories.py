from django.utils.datetime_safe import datetime
from ndptc.deliveries.defs import DeliveryStatus
from ndptc.deliveries.models import Delivery, TrainingMethod, DeliveryType, Evaluation, AfterActionReport
from ndptc.utilities.models import State, Country, Language
from ..courses.factories import CourseFactory
import factory
import pytz


class DeliveryFactory(factory.django.DjangoModelFactory):
    FACTORY_FOR = Delivery

    course = factory.SubFactory(CourseFactory)
    training_method = TrainingMethod.objects.get(code='M')
    delivery_type = DeliveryType.objects.get(code='C')
    address = '1000 First Street'
    address1 = 'Suite 102'
    city = 'Honolulu'
    state = State.objects.get(fema_code='HI')
    zip = '96811'
    country = Country.objects.get(fema_code='US')

    # Dates
    timezone = pytz.timezone('Pacific/Honolulu')
    start = datetime.utcnow().replace(tzinfo=timezone)
    end = datetime.utcnow().replace(tzinfo=timezone)
    pre_test_reminder = datetime.utcnow().replace(tzinfo=timezone)
    pre_test_close = datetime.utcnow().replace(tzinfo=timezone)
    post_test_close = datetime.utcnow().replace(tzinfo=timezone)
    evaluation_close = datetime.utcnow().replace(tzinfo=timezone)
    delivery_closed = datetime.utcnow().replace(tzinfo=timezone)
    registration_close = datetime.utcnow().replace(tzinfo=timezone)

    confirmation_code = 12345
    reported = False
    status = DeliveryStatus.ONLINE_REGISTRATION & DeliveryStatus.PRE_TEST & DeliveryStatus.POST_TEST & DeliveryStatus.EVALUATION
    hosting_agency = 'NDPTC'
    maximum_enrollment = None
    certificates_sent = False
    is_active = True
    language = Language.objects.get(code='en')


class EvaluationFactory(factory.django.DjangoModelFactory):
    FACTORY_FOR = Evaluation

    delivery = factory.SubFactory(DeliveryFactory)
    q1 = 1
    q2 = 2
    q3 = 3
    q4 = 4
    q5 = 5
    q6 = 0
    q7 = 1
    q8 = 2
    q9 = 3
    q10 = 4
    q11 = 5
    q12 = 0
    q13 = 1
    q14 = 2
    q15 = 3
    q16 = 4
    q17 = 5
    q18 = 0
    q19 = 1
    q20 = 2
    q21 = 3
    q22 = 4
    q23 = 5
    q24 = 'Question 24'
    q25 = 'Question 25'
    q26 = 'Question 26'
    q27 = 'Question 27'


class AfterActionReportFactory(factory.django.DjangoModelFactory):
    FACTORY_FOR = AfterActionReport

    delivery = factory.SubFactory(DeliveryFactory)
    participants_a = 1
    participants_b = 1
    facilities_c = 1
    facilities_d = 1
    materials_e = 1
    materials_f = 1
    materials_f_other = 'Materials F Other'
    materials_g_1 = 1
    materials_g_2 = 1
    materials_g_3 = 1
    materials_g_4 = 1
    materials_g_other = 'Materials G Other'
    materials_h = 1
    other_i = 1
    other_other = 'Other Other'
    instructor_a = 1
    instructor_a_conflict = 1
    instructor_a_explain = 'Instructor Explain'
    instructor_b = 1
    instructor_b_comments = 'Instructor Comments'
    course_a = 1
    course_b = 1
    course_b_comments = 'Course B Comments'
    course_c = 1
    course_d = 1
    course_e = 1
    course_f = 1
    course_f_comments = 'Course F Comments'
    admin_a = 1
    admin_b = 1
    admin_c = 1
    admin_d = 1
    admin_e = 1
    admin_comments = 'Admin Comments'
    general_a = 1
    general_b = 1
    general_c = 1
    general_d = 1
    general_comments_1 = 'General Comments 1'
    general_comments_2 = 'General Comments 2'
    general_comments_3 = 'General Comments 3'
    general_comments_4_1 = 'Y'
    general_comments_4_2 = 'General Comments 4'
    general_comments_5 = 'General Comments 5'
