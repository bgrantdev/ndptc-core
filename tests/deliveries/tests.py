from datetime import timedelta
from django.test import TestCase
from django.utils import timezone
from django.utils.datetime_safe import datetime
from django.utils.timezone import utc
from ndptc.deliveries.models import Delivery, Taught
from ndptc.participants.models import WaitList
from ndptc.utilities.models import State, Discipline, GovernmentLevel
from ndptc.instructors.models import InstructorType
from .factories import DeliveryFactory, EvaluationFactory, AfterActionReportFactory
from ..accounts.factories import DMSPersonFactory
from ..courses.factories import CourseFactory, TestFactory, QuestionFactory, AnswerFactory
from ..participants.factories import ParticipantFactory, TestResultFactory
from ..instructors.factories import InstructorEvaluationFactory


class DeliveryTests(TestCase):

    def setUp(self):
        now = datetime.utcnow().replace(tzinfo=timezone.utc)
        one_day = timedelta(days=1)
        self.yesterday = now - one_day
        self.tomorrow = now + one_day

    def code_required_test(self):
        delivery = DeliveryFactory()
        delivery.code_required = False
        self.assertFalse(delivery.code_required)
        delivery.code_required = True
        self.assertTrue(delivery.code_required)

    def evaluation_test(self):
        delivery = DeliveryFactory()
        delivery.evaluation = False
        self.assertFalse(delivery.evaluation)
        delivery.evaluation = True
        self.assertTrue(delivery.evaluation)

    def evaluation_open_test(self):
        delivery = DeliveryFactory(start=self.yesterday, evaluation_close=self.tomorrow)
        delivery.evaluation = False
        self.assertFalse(delivery.evaluation_open)
        delivery.evaluation = True
        self.assertTrue(delivery.evaluation_open)
        delivery.start = self.tomorrow
        delivery.evaluation_close = self.yesterday
        self.assertFalse(delivery.evaluation_open)

    def pre_test_test(self):
        delivery = DeliveryFactory()
        delivery.pre_test = False
        self.assertFalse(delivery.pre_test)
        delivery.pre_test = True
        self.assertTrue(delivery.pre_test)

    def pre_test_open_test(self):
        delivery = DeliveryFactory(start=self.yesterday, pre_test_close=self.tomorrow)
        delivery.pre_test = False
        self.assertFalse(delivery.pre_test_open)
        delivery.pre_test = True
        self.assertTrue(delivery.pre_test_open)
        delivery.start = self.tomorrow
        delivery.pre_test_close = self.yesterday
        self.assertFalse(delivery.pre_test_open)

    def post_test_test(self):
        delivery = DeliveryFactory()
        delivery.post_test = False
        self.assertFalse(delivery.post_test)
        delivery.post_test = True
        self.assertTrue(delivery.post_test)

    def post_test_open_test(self):
        delivery = DeliveryFactory(start=self.yesterday, post_test_close=self.tomorrow)
        delivery.post_test = False
        self.assertFalse(delivery.post_test_open)
        delivery.post_test = True
        self.assertTrue(delivery.post_test_open)
        delivery.start = self.tomorrow
        delivery.post_test_close = self.yesterday
        self.assertFalse(delivery.post_test_open)

    def post_test_open_test(self):
        delivery = DeliveryFactory(start=self.yesterday, post_test_close=self.yesterday)
        delivery.post_test = False
        self.assertFalse(delivery.post_test_closed)
        delivery.post_test = True
        self.assertTrue(delivery.post_test_closed)
        delivery.start = self.tomorrow
        delivery.post_test_close = self.tomorrow
        self.assertFalse(delivery.post_test_closed)

    def online_test(self):
        delivery = DeliveryFactory()
        delivery.online = False
        self.assertFalse(delivery.online)
        delivery.online = True
        self.assertTrue(delivery.online)

    def city_state_test(self):
        delivery = DeliveryFactory(city='Honolulu', state=State.objects.get(fema_code='HI'))
        self.assertEqual('Honolulu, Hawaii', delivery.city_state())

    def pretty_address_test(self):
        delivery = DeliveryFactory(address='123 St', address1=None, city='Honolulu',
                                   state=State.objects.get(fema_code='HI'), zip='12345')
        self.assertEqual('123 St\nHonolulu, Hawaii. 12345', delivery.pretty_address())
        delivery = DeliveryFactory(address='123 St', address1='Suite 123', city='Honolulu',
                                   state=State.objects.get(fema_code='HI'), zip='12345')
        self.assertEqual('123 St\nSuite 123\nHonolulu, Hawaii. 12345', delivery.pretty_address())

    def has_seats_available_test(self):
        delivery = DeliveryFactory(maximum_enrollment=None)
        self.assertTrue(delivery.has_seats_available())
        delivery.maximum_enrollment = 10
        self.assertTrue(delivery.has_seats_available())
        delivery.maximum_enrollment = 0
        self.assertFalse(delivery.has_seats_available())
        delivery.maximum_enrollment = 2
        ParticipantFactory(delivery=delivery)
        self.assertTrue(delivery.has_seats_available())

    def seats_available_test(self):
        delivery = DeliveryFactory(maximum_enrollment=None)
        self.assertEqual('--', delivery.seats_available())
        delivery.maximum_enrollment = 10
        self.assertEqual(10, delivery.seats_available())
        delivery.maximum_enrollment = 0
        self.assertEqual('Full', delivery.seats_available())
        delivery.maximum_enrollment = 2
        ParticipantFactory(delivery=delivery)
        self.assertEqual(1, delivery.seats_available())

    def set_active_test(self):
        delivery = DeliveryFactory()
        delivery.set_active(False)
        self.assertFalse(delivery.is_active)
        delivery.set_active(True)
        self.assertTrue(delivery.is_active)

    def create_participant_test(self):
        delivery = DeliveryFactory()
        person = DMSPersonFactory()
        participant = delivery.create_participant(person)
        self.assertEqual(person, participant[0].person)
        participant = delivery.create_participant(person)
        self.assertEqual(None, participant[0])

    def register_for_course_test(self):
        delivery = DeliveryFactory(registration_close=self.yesterday)
        person = DMSPersonFactory()
        participant = delivery.register_for_course(person)
        self.assertEqual(None, participant[0])
        delivery.registration_close = self.tomorrow
        participant = delivery.register_for_course(person)
        self.assertEqual(person, participant[0].person)
        delivery.maximum_enrollment = 0
        participant = delivery.register_for_course(DMSPersonFactory())
        self.assertEqual(WaitList, type(participant[0]))
        try:
            delivery.register_for_course(None)
            self.assertTrue(False)
        except AssertionError:
            self.assertTrue(True)

    def push_wait_list_test(self):
        delivery = DeliveryFactory()
        person = DMSPersonFactory()
        wait_list = delivery.push_wait_list(person)
        self.assertEqual(person, wait_list[0].person)
        wait_list = delivery.push_wait_list(person)
        self.assertEqual(None, wait_list[0])

    def pop_wait_list_test(self):
        delivery = DeliveryFactory()
        person = DMSPersonFactory()
        wait_list = delivery.push_wait_list(person)
        self.assertEqual(1, WaitList.objects.filter(delivery=delivery).count())
        wait_list = delivery.pop_wait_list(wait_list[0].pk)
        self.assertEqual(person, wait_list[0].person)
        self.assertEqual(0, WaitList.objects.filter(delivery=delivery).count())
        person2 = DMSPersonFactory()
        delivery.push_wait_list(person2)
        delivery.push_wait_list(person)
        self.assertEqual(2, WaitList.objects.filter(delivery=delivery).count())
        wait_list = delivery.pop_wait_list()
        self.assertEqual(person2, wait_list[0].person)
        self.assertEqual(1, WaitList.objects.filter(delivery=delivery).count())

    def deliveries_by_course_report_test(self):
        course1 = CourseFactory()
        course2 = CourseFactory()
        DeliveryFactory(course=course1)
        DeliveryFactory(course=course1)
        DeliveryFactory(course=course2)
        report = Delivery.deliveries_by_course_report()
        self.assertEqual(course1, report[0]['course'])
        self.assertEqual(2, report[0]['deliveries'])
        self.assertEqual(course2, report[1]['course'])
        self.assertEqual(1, report[1]['deliveries'])

    def deliveries_by_region_report_test(self):
        [DeliveryFactory(state=State.objects.filter(fema_region=i)[0]) for i in range(1, 11)]
        report = Delivery.deliveries_by_region_report()
        [self.assertEqual(1, report[i]['deliveries']) for i in range(0, 10)]

    def discipline_report_test(self):
        delivery = DeliveryFactory()
        [ParticipantFactory(person__discipline=discipline, delivery=delivery) for discipline in
         Discipline.objects.all()]
        [self.assertEqual(1, report[1]) for report in delivery.discipline_report()]

    def government_level_report_test(self):
        delivery = DeliveryFactory()
        [ParticipantFactory(person__government_level=government_level, delivery=delivery) for government_level in
         GovernmentLevel.objects.all()]
        [self.assertEqual(1, report[1]) for report in delivery.government_level_report()]

    def state_report_test(self):
        delivery = DeliveryFactory()
        [ParticipantFactory(person__state=state, delivery=delivery) for state in State.objects.all()]
        [self.assertEqual(1, report[1]) for report in delivery.state_report()]

    def evaluation_report_test(self):
        delivery = DeliveryFactory()
        EvaluationFactory(delivery=delivery)
        report = delivery.evaluation_report()
        for i, section in enumerate(report):
            if i == 0:
                self.assertEqual(1, section['results'][0][1])
                self.assertEqual(1, section['results'][1][2])
            if i == 1:
                self.assertEqual(1, section['results'][0][3])
                self.assertEqual(1, section['results'][1][4])
                self.assertEqual(1, section['results'][2][5])
                self.assertEqual(1, section['results'][3][0])
            if i == 2:
                self.assertEqual(1, section['results'][0][1])
                self.assertEqual(1, section['results'][1][2])
                self.assertEqual(1, section['results'][2][3])
                self.assertEqual(1, section['results'][3][4])
                self.assertEqual(1, section['results'][4][5])
                self.assertEqual(1, section['results'][5][0])
                self.assertEqual(1, section['results'][6][1])
                self.assertEqual(1, section['results'][7][2])
            if i == 3:
                self.assertEqual(1, section['results'][0][3])
                self.assertEqual(1, section['results'][1][4])
                self.assertEqual(1, section['results'][2][5])
                self.assertEqual(1, section['results'][3][0])
                self.assertEqual(1, section['results'][4][1])
            if i == 4:
                self.assertEqual(1, section['results'][0][2])
                self.assertEqual(1, section['results'][1][3])
                self.assertEqual(1, section['results'][2][4])
                self.assertEqual(1, section['results'][3][5])
            if i == 5:
                for i, comment in enumerate(section['results']):
                    self.assertEqual('Question {0}'.format(i+24), comment[0][0])

    def participants_report_test(self):
        delivery = DeliveryFactory()
        ParticipantFactory(is_active=False, delivery=delivery)
        ParticipantFactory(delivery=delivery)
        ParticipantFactory(attended=True, passed=True, delivery=delivery)
        ParticipantFactory(attended=True, passed=False, delivery=delivery)
        ParticipantFactory(pre_test=True, delivery=delivery)
        ParticipantFactory(pre_test=True, post_test=True, delivery=delivery)
        report = delivery.participants_report()
        self.assertEqual(5, report[0][1])
        self.assertEqual(3, report[1][1])
        self.assertEqual(4, report[2][1])
        self.assertEqual(2, report[3][1])
        self.assertEqual(1, report[4][1])
        self.assertEqual(1, report[5][1])

    def summary_certificates_report_test(self):
        delivery = DeliveryFactory()
        ParticipantFactory(delivery=delivery, person__email='')
        ParticipantFactory(attended=True, passed=True, delivery=delivery)
        ParticipantFactory(attended=True, passed=False, delivery=delivery)
        ParticipantFactory(pre_test=True, delivery=delivery)
        ParticipantFactory(pre_test=True, post_test=True, delivery=delivery)
        report = delivery.summary_certificates_report()
        self.assertEqual(1, report[0][1])
        self.assertEqual(4, report[1][1])
        self.assertEqual(1, report[2][1])
        self.assertEqual(0, report[3][1])
        self.assertEqual(4, report[4][1])

    def has_after_action_test(self):
        delivery = DeliveryFactory()
        self.assertFalse(delivery.has_after_action())
        AfterActionReportFactory(delivery=delivery)
        self.assertTrue(delivery.has_after_action())

    def year_manager_test(self):
        year1 = datetime(2012, 9, 30, tzinfo=utc)
        year2 = datetime(2012, 10, 1, tzinfo=utc)
        DeliveryFactory(start=year1, end=year1)
        DeliveryFactory(start=year2, end=year2)
        self.assertEqual(1, Delivery.objects.year(2012).count())
        self.assertEqual(1, Delivery.objects.year(2013).count())
        # TODO: This could test the timezone's better

    def instructor_evaluation_report_test(self):
        delivery = DeliveryFactory()
        instructor_evaluation = InstructorEvaluationFactory(delivery=delivery)
        Taught.objects.create(delivery=delivery, instructor=instructor_evaluation.instructor,
                              instructor_type=InstructorType.objects.get(code='IN'))
        report = delivery.instructor_evaluation_report()
        for answer in report[0][1]['answers']:
            self.assertEqual(1, answer[1])
        self.assertEqual('Comment', report[0][1]['comments'][0][0])

    def test_report_test(self):
        delivery = DeliveryFactory()
        self.assertEqual(None, delivery.test_report('FOO'))
        test = TestFactory()
        question = QuestionFactory(test=test)
        question2 = QuestionFactory(test=test)
        answer1 = AnswerFactory(question=question, correct=True)
        answer2 = AnswerFactory(question=question, correct=False)
        AnswerFactory(question=question2)
        delivery.tests.add(test)
        TestResultFactory(participant__delivery=delivery, question=question, answer=answer1)
        TestResultFactory(participant__delivery=delivery, question=question, answer=answer2)
        report = delivery.test_report('PRE')
        answers = report[0]['answers']
        self.assertEqual('50.0', answers[0]['percentage'])
        self.assertEqual(1, answers[0]['num_selected'])
        self.assertEqual('50.0', answers[1]['percentage'])
        self.assertEqual(1, answers[1]['num_selected'])
        answers = report[1]['answers']
        self.assertEqual('0.0', answers[0]['percentage'])
        self.assertEqual(0, answers[0]['num_selected'])

