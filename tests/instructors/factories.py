from ndptc.instructors.models import Instructor, EmploymentStatus, InstructorEvaluation, Certification
from ..accounts.factories import DMSPersonFactory
from ..courses.factories import CourseFactory
from ..deliveries.factories import DeliveryFactory
import factory


class InstructorFactory(factory.django.DjangoModelFactory):
    FACTORY_FOR = Instructor
    person = factory.SubFactory(DMSPersonFactory)
    employment_status = EmploymentStatus.objects.all()[0]
    is_active = True


class InstructorEvaluationFactory(factory.django.DjangoModelFactory):
    FACTORY_FOR = InstructorEvaluation

    instructor = factory.SubFactory(InstructorFactory)
    delivery = factory.SubFactory(DeliveryFactory)
    question1 = 1
    question2 = 1
    question3 = 1
    question4 = 1
    question5 = 1
    question6 = 1
    comment = 'Comment'


class CertificationFactory(factory.django.DjangoModelFactory):
    FACTORY_FOR = Certification

    instructor = factory.SubFactory(InstructorFactory)
    course = factory.SubFactory(CourseFactory)
    subject_expert = False
    emergency_management = False
    is_active = True