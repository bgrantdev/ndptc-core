from django_nose import FastFixtureTestCase
from ..deliveries.factories import DeliveryFactory
from .factories import InstructorEvaluationFactory, InstructorFactory, CertificationFactory


class InstructorTests(FastFixtureTestCase):

    def str_test(self):
        instructor = InstructorFactory(person__first_name='test', person__last_name='person')
        self.assertEqual('person, test', str(instructor))
        self.assertEqual('test person', instructor.natural_name())

    def evaluation_report_test(self):
        instructor = InstructorFactory()
        certification = CertificationFactory(instructor=instructor)
        delivery = DeliveryFactory(course=certification.course)
        InstructorEvaluationFactory(instructor=instructor, delivery=delivery)
        report = instructor.evaluation_report()
        for answer in report[0][1]['answers']:
            self.assertEqual(1, answer[1])
        self.assertEqual('Comment', report[0][1]['comments'][0][0])
