from django.utils.datetime_safe import datetime
from django.utils.timezone import UTC
from ndptc.courses.models import TestType
from ndptc.participants.models import Participant, TestResult, TestScore
from ..accounts.factories import DMSPersonFactory
from ..courses.factories import QuestionFactory, AnswerFactory
from ..deliveries.factories import DeliveryFactory
import factory


class ParticipantFactory(factory.django.DjangoModelFactory):
    FACTORY_FOR = Participant
    delivery = factory.SubFactory(DeliveryFactory)
    person = factory.SubFactory(DMSPersonFactory)
    pre_test = False
    post_test = False
    attended = False
    evaluation = False
    passed = False
    registered = datetime.utcnow().replace(tzinfo=UTC())
    is_active = True


class TestResultFactory(factory.django.DjangoModelFactory):
    FACTORY_FOR = TestResult
    participant = factory.SubFactory(ParticipantFactory)
    question = factory.SubFactory(QuestionFactory)
    answer = factory.SubFactory(AnswerFactory)
    attempt = 1
    is_latest = True
    is_active = True


class TestScoreFactory(factory.django.DjangoModelFactory):
    FACTORY_FOR = TestScore
    type = TestType.objects.get(pk="PRE")
    score = 100
    attempt = 1
    is_latest = True
    is_active = True

