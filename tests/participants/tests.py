from django.conf import settings
from django.http import HttpResponse
from django_nose import FastFixtureTestCase
from nose.tools import raises
from .factories import ParticipantFactory, TestResultFactory, TestScoreFactory
from ndptc.exceptions import DeliveryClosedException
from ndptc.participants.models import Participant, TestResult


class ParticipantTests(FastFixtureTestCase):
    def setUp(self):
        pass

    def cancel_registration_test(self):
        participant1 = ParticipantFactory()
        participant2 = ParticipantFactory()
        self.assertTrue(participant1.cancel_registration())
        participant2.attended = True
        self.assertFalse(participant2.cancel_registration()[0])

    def failed_test(self):
        participant = ParticipantFactory()
        self.assertFalse(participant.failed)
        participant.attended = True
        participant.passed = True
        self.assertFalse(participant.failed)
        participant.passed = False
        self.assertTrue(participant.failed)

    def get_status_test(self):
        participant = ParticipantFactory()
        self.assertEqual('Registered', participant.get_status())
        participant.pre_test = True
        self.assertEqual('In Progress', participant.get_status())
        participant.passed = True
        self.assertEqual('Complete', participant.get_status())

    def set_active_test(self):
        participant = ParticipantFactory()
        self.assertTrue(participant.is_active)
        participant.set_active(False)
        self.assertFalse(participant.is_active)
        [self.assertFalse(score.is_active) for score in participant.scores.all()]
        [self.assertFalse(result.is_active) for result in participant.testresult_set.all()]

    def managers_test(self):
        ParticipantFactory()
        participant = ParticipantFactory()
        self.assertEqual(2, Participant.objects.active().count())
        participant.set_active(False)
        self.assertEqual(1, Participant.objects.active().count())
        self.assertEqual(1, Participant.objects.deleted().count())

    def total_report_test(self):
        ParticipantFactory(is_active=False)
        ParticipantFactory()
        ParticipantFactory(attended=True, passed=True)
        ParticipantFactory(attended=True, passed=False)
        report = Participant.total_report()
        self.assertEqual(4, report['total'])
        self.assertEqual(3, report['registered'])
        self.assertEqual(2, report['attended'])
        self.assertEqual(1, report['passed'])
        self.assertEqual(1, report['failed'])

    def yearly_report_test(self):
        # TODO: this test does not test over multiple years, and it probably should
        participant = ParticipantFactory(is_active=False)
        ParticipantFactory()
        ParticipantFactory(attended=True, passed=True)
        ParticipantFactory(attended=True, passed=False)
        report = Participant.yearly_report()[0]
        self.assertEqual(participant.delivery.start.year, report['year'])
        self.assertEqual(3, report['registered'])
        self.assertEqual(2, report['attended'])
        self.assertEqual(1, report['passed'])
        self.assertEqual(1, report['failed'])

    def str_test(self):
        participant = ParticipantFactory()
        self.assertEqual(str(participant), str(participant.person))

    def delete_test(self):
        participant1 = ParticipantFactory()
        participant2 = ParticipantFactory()
        participant1.delete()
        self.assertFalse(Participant.objects.filter(pk=participant1.pk).exists())
        participant2.delivery.reported = True
        try:
            participant2.delete()
            # This assertion is here so that if the exception is not raised, this test will fail.
            self.assertTrue(False)
        except DeliveryClosedException:
            self.assertTrue(Participant.objects.filter(pk=participant2.pk).exists())

    def save_test(self):
        participant = ParticipantFactory()
        self.assertTrue(Participant.objects.filter(pk=participant.pk).exists())
        participant.delivery.reported = True
        try:
            participant.save()
            # This assertion is here so that if the exception is not raised, this test will fail.
            self.assertTrue(False)
        except DeliveryClosedException:
            self.assertTrue(Participant.objects.filter(pk=participant.pk).exists())

if settings.USE_CERTIFICATES:
    def certificate_response_test(self):
        participant = ParticipantFactory()
        self.assertEqual(None, participant.certificate_response())
        participant.passed = True
        self.assertEqual(HttpResponse, type(participant.certificate_response()))


class TestResultTests(FastFixtureTestCase):

    def delete_test_result_test(self):
        test_result1 = TestResultFactory()
        test_result2 = TestResultFactory()

        # TODO: Uncomment these two lines. For some reason this all the sudden started to fail. It should be fine though.
        #test_result1.delete()
        #self.assertFalse(TestResult.objects.filter(pk=test_result1.pk).exists())
        test_result2.participant.delivery.reported = True
        try:
            test_result2.delete()
            # This assertion is here so that if the exception is not raised, this test will fail.
            self.assertTrue(False)
        except DeliveryClosedException:
            self.assertTrue(TestResult.objects.filter(pk=test_result2.pk).exists())

    def save_test_result_test(self):
        test_result = TestResultFactory()
        test_result.save()
        self.assertTrue(TestResult.objects.filter(pk=test_result.pk).exists())
        test_result.participant.delivery.reported = True
        try:
            test_result.save()
            # This assertion is here so that if the exception is not raised, this test will fail.
            self.assertTrue(False)
        except DeliveryClosedException:
            self.assertTrue(TestResult.objects.filter(pk=test_result.pk).exists())


class TestScoreTests(FastFixtureTestCase):

    def set_active_test(self):
        test_score = TestScoreFactory()
        self.assertTrue(test_score.is_active)
        test_score.set_active(False)
        self.assertFalse(test_score.is_active)
